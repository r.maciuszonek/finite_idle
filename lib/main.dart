import 'package:finiteidle/app/finite_idle.dart';
import 'package:flutter/material.dart';

void main() async {
  FiniteIdle.preInitSetup;
  runApp(const FiniteIdle());
}
