import 'package:finiteidle/futures/main/presentation/controllers/main_page_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MainPage extends GetResponsiveView<MainPageController> {
  MainPage({Key? key}) : super(key: key);

  Widget desktopContent() {
    return Container(
      color: Colors.red,
    );
  }

  Widget phoneContent() {
    return Container(
      color: Colors.purple,
    );
  }

  Widget tabletContent() {
    return Container(
      color: Colors.green,
    );
  }

  Widget watchContent() {
    return Container(
      color: Colors.yellow,
    );
  }

  // Future<bool> onWillPop() async {
  //   NavigationMenuDrawer navigationMenu = const NavigationMenuDrawer();
  //   navigationMenu.controller.selectedItemAction(0);
  //   return true;
  // }
}
