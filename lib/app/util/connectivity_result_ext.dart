import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:finiteidle/app/translation/msg.dart';

extension ConnectivityResultExt on ConnectivityResult {
  get label => [
        Msg.nameConnectivityResultBluetooth.tr,
        Msg.nameConnectivityResultWifi.tr,
        Msg.nameConnectivityResultEthernet.tr,
        Msg.nameConnectivityResultMobile.tr,
        Msg.nameConnectivityResultNone.tr,
      ][index];

  get color => [
        Colors.blue,
        Colors.green,
        Colors.cyanAccent,
        Colors.lightGreen,
        Colors.red,
      ][index];
}
