extension StringUtils on String {
  bool get isNumeric => _isNumeric(this);
  bool get isInt => _isInt(this);
  bool get isDouble => _isDouble(this);

  bool _isNumeric(String s) {
    return _isInt(s) || _isDouble(s);
  }

  bool _isInt(String s) {
    s.trim();
    if (s.isEmpty) {
      return false;
    }
    return int.tryParse(s) != null;
  }

  bool _isDouble(String s) {
    s.trim();
    if (s.isEmpty) {
      return false;
    }
    return double.tryParse(s) != null;
  }
}
