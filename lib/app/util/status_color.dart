import 'package:flutter/material.dart';

enum StatusColor {
  cNew,
  cStarted,
  cInProgress,
  cReady,
  cAccepted,
  cDamaged,
  cRejected,
  cFailure,
  cCompleted,
}

extension StatusColorExt on StatusColor {
  static StatusColor? getStatusColorByStringName(String? colorName) {
    if (colorName == null || colorName.isEmpty) {
      return null;
    }
    switch (colorName) {
      case 'new':
      case 'color-new':
        return StatusColor.cNew;
      case 'started':
      case 'color-started':
        return StatusColor.cStarted;
      case 'in-progress':
      case 'color-in-progress':
        return StatusColor.cInProgress;
      case 'ready':
      case 'color-ready':
        return StatusColor.cReady;
      case 'accepted':
      case 'color-accepted':
        return StatusColor.cAccepted;
      case 'damaged':
      case 'color-damaged':
        return StatusColor.cDamaged;
      case 'rejected':
      case 'color-rejected':
        return StatusColor.cRejected;
      case 'failure':
      case 'color-failure':
        return StatusColor.cFailure;
      case 'completed':
      case 'color-completed':
        return StatusColor.cCompleted;
      default:
        return null;
    }
  }

  String get name => const [
        'new',
        'started',
        'in progress',
        'ready',
        'accepted',
        'damaged',
        'rejected',
        'failure',
        'completed',
      ][index];
  String get label => [
        'label.color.new',
        'label.color.started',
        'label.color.in-progress',
        'label.color.ready',
        'label.color.accepted',
        'label.color.damaged',
        'label.color.rejected',
        'label.color.failure',
        'label.color.completed',
      ][index];

  Color? get bgColor => {
        StatusColor.cNew: const Color(0xFFA7EEFF),
        StatusColor.cStarted: const Color(0xFFFFFF9B),
        StatusColor.cInProgress: const Color(0xFFACEAAC),
        StatusColor.cReady: const Color(0xff4935f9),
        StatusColor.cAccepted: const Color(0xffb0006c),
        StatusColor.cDamaged: const Color(0xffdb6a20),
        StatusColor.cRejected: const Color(0xff823a00),
        StatusColor.cFailure: const Color(0xffff0101),
        StatusColor.cCompleted: const Color(0xff1eff00),
      }[index];

  Color? get textColor => {
        StatusColor.cNew: const Color(0xFFA7EEFF),
        StatusColor.cStarted: const Color(0xFFFFFF9B),
        StatusColor.cInProgress: const Color(0xFFACEAAC),
        StatusColor.cReady: const Color(0xff4935f9),
        StatusColor.cAccepted: const Color(0xffb0006c),
        StatusColor.cDamaged: const Color(0xffdb6a20),
        StatusColor.cRejected: const Color(0xff823a00),
        StatusColor.cFailure: const Color(0xffff0101),
        StatusColor.cCompleted: const Color(0xff1eff00),
      }[index];
}
