import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  const Failure([List properties = const <dynamic>[]]);
}

class ServerFailure extends Failure {
  final int statusCode;
  const ServerFailure(this.statusCode);
  @override
  List<Object> get props => [];
}

class DatabaseFailure extends Failure {
  @override
  List<Object> get props => [];
}

class LoginFailure extends Failure {
  @override
  List<Object> get props => [];
}

class UnexpectedFailure extends Failure {
  @override
  List<Object> get props => [];
}

class AccessFailure extends Failure {
  @override
  List<Object> get props => [];
}
