import 'package:equatable/equatable.dart';

abstract class Success extends Equatable {
  const Success([List properties = const <dynamic>[]]);
}

class SaveSuccess extends Success {
  @override
  List<Object> get props => [];
}

class LoadSuccess extends Success {
  @override
  List<Object> get props => [];
}

class DeleteSuccess extends Success {
  @override
  List<Object> get props => [];
}

class AccessSuccess extends Success {
  @override
  List<Object> get props => [];
}
