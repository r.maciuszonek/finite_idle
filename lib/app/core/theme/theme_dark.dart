import 'package:flutter/material.dart';

class ThemeDark {
  static ThemeData get theme => ThemeData.dark(useMaterial3: true).copyWith(
        primaryColor: const Color(0xff424242),
      );
}
