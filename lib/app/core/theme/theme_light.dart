import 'package:flutter/material.dart';

class ThemeLight {
  static ThemeData get theme => ThemeData.light(useMaterial3: true).copyWith(
        primaryColor: Colors.blue,
        scaffoldBackgroundColor: Colors.grey[300],
      );
}
