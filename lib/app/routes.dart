import 'package:finiteidle/futures/main/presentation/bindings/main_page_binding.dart';
import 'package:finiteidle/futures/main/presentation/pages/main_page.dart';
import 'package:get/get_navigation/get_navigation.dart';

class Routes {
  static const main = '/';
  static const authentication = '/authentication';
}

get appRoutes => [
      GetPage(
        name: Routes.main,
        page: () => MainPage(),
        binding: MainPageBinding(),
        transition: null,
        transitionDuration: const Duration(milliseconds: 500),
      ),
      // GetPage(
      //   name: Routes.authentication,
      //   page: AuthenticationPage(),
      //   binding: AauthenticationPageBinding(),
      //   transition: Transition.fadeIn,
      //   transitionDuration: const Duration(milliseconds: 500),
      // ),
    ];
