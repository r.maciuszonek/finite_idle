import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class UnknownRoutePage extends GetView {
  const UnknownRoutePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          Get.currentRoute.toString(),
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
              "Error",
              style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(16.0),
            child: Text(
              "Unknown page route",
              style: TextStyle(fontSize: 22),
              textAlign: TextAlign.center,
            ),
          ),
          FittedBox(
            fit: BoxFit.fitHeight,
            child: Padding(
              padding: const EdgeInsets.all(32.0),
              child: Lottie.asset(
                'assets/lottie/unknown-page-404-outcome-page.json',
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
