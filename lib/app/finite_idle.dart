import 'package:finiteidle/app/common/widgets/unknown_route_page.dart';
import 'package:finiteidle/app/finite_idle_controller.dart';
import 'package:finiteidle/app/translation/msg.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:get_storage/get_storage.dart';

class FiniteIdle extends GetView<FiniteIdleController> {
  const FiniteIdle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.lazyPut<FiniteIdleController>(() => FiniteIdleController());
    return GetMaterialApp(
      title: Msg.appTitle.tr,
      translations: controller.languages,
      locale: Get.deviceLocale,
      fallbackLocale: const Locale('en', 'EN'),
      themeMode: ThemeMode.system,
      theme: controller.theme,
      darkTheme: controller.theme,
      unknownRoute: GetPage(
        name: '/404-not_found',
        page: () => const UnknownRoutePage(),
      ),
      getPages: controller.pages,
      onReady: controller.init,
      home: const MainPage(),
    );
  }

  static preInitSetup() async {
    // WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
    // FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    await GetStorage.init();
    await _FiniteIdle.initHiveFlutter();
    await _FiniteIdle.initServices(packageInfo);
  }
}

class _FiniteIdle {
  static Future<void> initServices(PackageInfo packageInfo) async {
    // await Get.putAsync(() => SettingsService().init());
    // await Get.putAsync(() => NetworkManagerService().init());
    // await Get.putAsync(() => ConnectionManagerService().init());
    // await Get.putAsync(() => AuthenticationService().init());
  }

  static Future<void> initHiveFlutter() async {
    await Hive.initFlutter();
    // Hive
    //   ..registerAdapter(FileTransferStatusAdapter())
    //   ..registerAdapter(TransferTypeAdapter());
  }
}

class MainPage extends GetView {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Get.put(NavigationMenuController());
    // Get.put(NavigationSidebarXController());
    // initTheme();
    // FlutterNativeSplash.remove();
    // Get.offAndToNamed(Routes.main);
    return const Scaffold();
  }
}
