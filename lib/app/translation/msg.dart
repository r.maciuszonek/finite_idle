class Msg {
  static const appTitle = 'appTitle';

  static const nameConnectivityResultBluetooth =
      'nameConnectivityResultBluetooth';
  static const nameConnectivityResultWifi = 'nameConnectivityResultWifi';
  static const nameConnectivityResultEthernet =
      'nameConnectivityResultEthernet';
  static const nameConnectivityResultMobile = 'nameConnectivityResultMobile';
  static const nameConnectivityResultNone = 'nameConnectivityResultNone';
}
