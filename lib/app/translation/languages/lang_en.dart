import '../msg.dart';
import '../base_language.dart';

class LangEn implements BaseLanguage {
  @override
  Map<String, String> get map => {
        Msg.appTitle: 'Finite Idle',
      };
}
