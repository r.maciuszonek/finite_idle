import '../msg.dart';
import '../base_language.dart';

class LangPl implements BaseLanguage {
  @override
  Map<String, String> get map => {
        Msg.appTitle: 'Finite Idle',
        Msg.nameConnectivityResultBluetooth: 'Bluetooth',
        Msg.nameConnectivityResultWifi: 'WiFi',
        Msg.nameConnectivityResultEthernet: 'Ethernet',
        Msg.nameConnectivityResultMobile: 'Dane mobilne',
        Msg.nameConnectivityResultNone: 'Brak połączenia',
      };
}
