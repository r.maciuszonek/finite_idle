import 'package:finiteidle/app/translation/base_language.dart';
import 'package:finiteidle/app/translation/languages/lang_en.dart';
import 'package:finiteidle/app/translation/languages/lang_pl.dart';
import 'package:get/get.dart';

class Languages extends Translations {
  Languages._constructor({required this.langEn, required this.langPl});
  static final Languages _instance =
      Languages._constructor(langEn: LangEn(), langPl: LangPl());
  final BaseLanguage langPl;
  final BaseLanguage langEn;

  factory Languages() {
    return _instance;
  }

  static Languages all() {
    return _instance;
  }

  @override
  Map<String, Map<String, String>> get keys => {
        'en_EN': langEn.map,
        'pl_PL': langPl.map,
      };
}
