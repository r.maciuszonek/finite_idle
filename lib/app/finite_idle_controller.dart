import 'package:finiteidle/app/core/theme/theme_dark.dart';
import 'package:finiteidle/app/core/theme/theme_light.dart';
import 'package:finiteidle/app/routes.dart';
import 'package:finiteidle/app/translation/languages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// TODO: move to another class/ settings feature
enum SettingsTheme { light, dark, system }

class FiniteIdleController extends GetxController {
  final Languages languages = Languages();
  get pages => appRoutes;
  get init => _init;

  // TODO: move to service
  Rx<SettingsTheme> settingsTheme = SettingsTheme.system.obs;
  ThemeData get theme => _getCurrentTheme();
  ThemeData _getCurrentTheme() {
    switch (settingsTheme.value) {
      case SettingsTheme.system:
        return Get.isDarkMode ? ThemeDark.theme : ThemeLight.theme;
      case SettingsTheme.light:
        return ThemeLight.theme;
      case SettingsTheme.dark:
        return ThemeDark.theme;
    }
  }

  _init() {
    // Get.find<AuthenticationService>().initNavigationListener();
    // Get.put(NavigationMenuController());
    // Get.put(NavigationSidebarXController());
  }
}
